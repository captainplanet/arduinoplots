#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "qcustomplot.h"
#include <QSerialPort>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    QCustomPlot *customPlot;
    QSerialPort* port;

private:
    Ui::MainWindow *ui;
private slots:
    void newData();
    void on_btnOpenPort_clicked();
};

#endif // MAINWINDOW_H
