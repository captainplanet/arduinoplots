
#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->statusBar->setStyleSheet("border-top:1px solid black;background-color:grey; color:black;");
    ui->statusBar->showMessage("Everything's ok");
    customPlot = ui->widget;
    customPlot->addGraph();
    customPlot->graph(0)->setPen(QPen(QBrush(QColor(0,0,164)),2));
    customPlot->graph(0)->setName("Yaw");

    customPlot->addGraph();
    customPlot->graph(1)->setPen(QPen(QBrush(QColor(0,164,0)),2));
    customPlot->graph(1)->setName("Pitch");

    customPlot->addGraph();
    customPlot->graph(2)->setPen(QPen(QBrush(QColor(164,0,0)),2));
    customPlot->graph(2)->setName("Roll");

    customPlot->legend->setVisible(true);
    customPlot->axisRect()->insetLayout()->setInsetAlignment(0, Qt::AlignLeft|Qt::AlignBottom);

    port = new QSerialPort(this);
    connect(port,SIGNAL(readyRead()),this,SLOT(newData()));

}

void MainWindow::newData()
{

    static bool state = false;
    static bool readyRead = false;
    static int bytesCount = 0;
    static int baseCount = 0;
    uchar base = 0;
    static float mData[10];
    while (port->bytesAvailable() > 0)
    {
        //
//        port->read((char*)mData,44);
        //qDebug() << readyRead << " " << port->bytesAvailable();
        if (!readyRead)
        {
            port->read((char*)&base,1);
            qDebug() << hex<< (int)base;
            if (base == 0xFF)
            {
                ++baseCount;
                //qDebug() << baseCount;
            }
            else
                baseCount = 0;
            if (baseCount == 4)
            {
                readyRead = true;
                baseCount = 0;
            }
        }

        if (readyRead)
            if (port->bytesAvailable() > 3)
            {
                port->read((char*)mData+bytesCount*4,4);
                ++bytesCount;
                if (bytesCount == 10)
                {
                    bytesCount = 0;
                    readyRead = false;
                    state = true;
                }
            }
            else
                break;
//        while (base != 0xffffffff)
//        {
//            qDebug() << port->read((char*)&base,4);
//        }


      //  qDebug() << mData[1];
        //QString str = port->readLine();
        //QStringList strLst = str.split(" ");
//        for (int i=0;i<strLst.size();++i)
//        {
//            mData[i] = strLst[i].toDouble();
//        }
        if (state)
        {
            //qDebug() << mData[9];
            customPlot->graph(0)->addData(mData[9],mData[1]);
            customPlot->graph(1)->addData(mData[9],mData[2]);
            customPlot->graph(2)->addData(mData[9],mData[3]);

            customPlot->rescaleAxes();
            customPlot->replot();
            state = false;
        }
        //else
        //    state = !state;
    }
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_btnOpenPort_clicked()
{
    static bool state = false;
    if (!state)
    {
        port->setPortName(ui->tbPortName->text());
        if (port->open(QIODevice::ReadWrite))
        {
           port->setBaudRate(QSerialPort::Baud115200);
           port->setDataBits(QSerialPort::Data8);
           port->setFlowControl(QSerialPort::NoFlowControl);
           port->setParity(QSerialPort::NoParity);
           port->setStopBits(QSerialPort::OneStop);
           ui->statusBar->showMessage("Opened!");
           ui->btnOpenPort->setStyleSheet("background-color:green;color:black;");
           state = !state;
        }
        else
            ui->statusBar->showMessage("Port's not opened :(");
    }
    else
    {
        if (port->isOpen())
        {
            port->close();
            ui->statusBar->showMessage("Closed!");
            ui->btnOpenPort->setStyleSheet("background-color:red;color:white;");
            state = !state;
        }
    }
}
